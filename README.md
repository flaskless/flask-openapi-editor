# Flask OpenAPI

OpenAPI Editor and generator as a flask app

## Usage

```python
import flask
app = flask.Flask(__name__)

SPEC = """
openapi: 3.0.0
info:
  title: Sample API
"""

import flask_openapi_editor

def on_save(new_spec):
    print(f'updating specification: {new_spec}')

openapi_editor = flask_openapi_editor.SwaggerEditor(app,spec=SPEC,editor_path='/editor',on_save=on_save)
```
### Lowerlevel API

```python
# pipelined methods for validating and saving the spec

@openapi_editor.on_save
def validate_spec(new_spec):
    # validate the spec

@openapi_editor.on_save
def validate_openalchemy_models(new_spec):
    # run code generator

@openapi_editor.on_save
def save_spec(new_spec):
    # write to storage

@app.route('/openapi/editor/<path:path>')
def editor(path):
    spec = spec_store.get(path)
    return openapi_editor.render(spec=spec)
```

### Usage in Custom Templates

```jinja

<head>
...
{{swagger_editor.css()}}
</head>
<body>
<div>
    ...
    {{swagger_editor(spec=...)}}
    ...
</div>
</body>

```

See the `examples` directory for an example

## Updating the editor distribution

1. clone the repo:

    git clone https://github.com/swagger-api/swagger-editor.git

2. copy the dist files

    cp -r swagger-editor/dist flask_openapi_editor/static/dist