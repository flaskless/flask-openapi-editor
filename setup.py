from setuptools import setup, find_packages

with open("README.md", "r") as fh:
    long_description = fh.read()

INSTALL_REQUIRES=[
"Flask",
]
setup(name='flask-openapi-editor',
      version='0.1.0dev0',
      description='Flask OpenAPI Editor',
      long_description=long_description,
      long_description_content_type="text/markdown",
      packages=find_packages(),
      include_package_data=True,
      install_requires=INSTALL_REQUIRES,
      python_requires='>=3.6',
      )
