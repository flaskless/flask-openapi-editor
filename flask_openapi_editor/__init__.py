
import flask
from markupsafe import Markup
DEFAULT_SPEC="""openapi: 3.0.1
info:
  title: Default Specification
  description: 'This is an online swagger codegen server.  You can find out more at https://github.com/swagger-api/swagger-codegen or on [irc.freenode.net, #swagger](http://swagger.io/irc/).'
  license:
    name: Apache 2.0
    url: 'http://www.apache.org/licenses/LICENSE-2.0.html'
  version: 3.0.23
servers:
  - url: /api
tags:
  - name: clients
  - name: servers
  - name: documentation
  - name: config
paths: {}
components:
  schemas: {}
"""

def spec_from_file(filepath,create=False):
    import os
    if not os.path.exists(filepath):
        os.makedirs(os.path.dirname(filepath),exist_ok=True)
        with open(filepath,'w') as f:
            f.write(DEFAULT_SPEC)

    with open(filepath) as f:
        return f.read()

class SwaggerEditor():

    def __init__(self, app=None, spec=None, editor=False, name=None,editor_path=None,on_save=None):

        self._editor = editor
        self._editor_path = editor_path
        self._spec = spec

        if name:
            self._name=name
        elif self._editor:
            self._name="swagger-editor"
        else:
            self._name="swagger-ui"

        self._on_save = on_save or self.validate_spec

        if app is not None:
            self.init_app(app)

    def on_save(self,f):
        self._on_save = f
        return f

    def validate_spec(self,s):
        flask.current_app.logger.info(f'Updating spec')
        return s

    @property
    def spec(self):
        if self._spec is None:
            spec_from_config = flask.current_app.config.get('SWAGGER_EDITOR_SPEC_URI')
            return spec_from_file(spec_from_config,create=True)
            
        if callable(self._spec):
            return self._spec()
        return self._spec

    def render(self,editor=False,spec=None):
        return flask.render_template('openapi/index.html',swagger_editor=self)

    def __call__(self,spec=None,editable=False):

        render_spec = spec or self.spec
        editable = editable or self.editable
        js = self._editor_js() if editable else self._ui_js()
        rendered = Markup(f'<div id="{self._safe_name}"></div>\n{js}')
        return rendered

    def init_app(self, app:flask.Flask):

        app.config.setdefault('SWAGGER_SERVE_LOCAL', True)
        app.config.setdefault('SWAGGER_EDITOR_PRESET_JS','./dist/swagger-editor-standalone-preset.js')
        app.config.setdefault('SWAGGER_EDITOR_BUNDLE_JS','./dist/swagger-editor-bundle.js')
        app.config.setdefault('SWAGGER_EDITOR_CSS', './dist/swagger-editor.css')
        app.config.setdefault('SWAGGER_UI_PRESET_JS','js/swagger-ui-standalone-preset.js')
        app.config.setdefault('SWAGGER_UI_BUNDLE_JS','js/swagger-ui-bundle.js')
        app.config.setdefault('SWAGGER_UI_CSS', 'css/swagger-ui.css')
        app.config.setdefault('SWAGGER_STATIC_URL_PATH',f'{app.static_url_path}/swagger_editor' if app.static_url_path else '/swagger_editor')
        app.config.setdefault('SWAGGER_EDITOR_SPEC_URI',None)

        blueprint = flask.Blueprint('swagger_editor', __name__,
                            template_folder='templates',
                            static_folder='static', 
                            static_url_path=app.config['SWAGGER_STATIC_URL_PATH']
                            )

        if self._editor_path is not None:    
            @blueprint.route(self._editor_path,methods=['GET','POST'])
            def editor():
                if flask.request.method == 'POST':
                    spec = flask.request.data.decode()
                    self._spec = spec
                    r = self._on_save(spec)
                    if r is not None:
                        return r
                    return 'Saved!',200
                    
                return self.render()

        app.register_blueprint(blueprint)

    @property
    def editable(self):
        return self._editor

    def css(self):
        if self.editable:
            return self._editor_css()
        return self._ui_css()

    def _editor_js(self):
        """
        Load Swagger Editor scripts
        """
        preset = flask.current_app.config.get('SWAGGER_EDITOR_PRESET_JS')
        bundle = flask.current_app.config.get('SWAGGER_EDITOR_BUNDLE_JS')

        if flask.current_app.config['SWAGGER_SERVE_LOCAL']:
            preset = flask.url_for('swagger_editor.static', filename=preset)
            bundle = flask.url_for('swagger_editor.static', filename=bundle)

        scripts = [f'<script src="{url}"></script>\n' for url in [preset, bundle]]

        custom = '''
            <script>
            window.onload = function() {
                // Build a system
                const editor = SwaggerEditorBundle({
                    dom_id: '#{{name}}',
                    layout: "StandaloneLayout",
                    presets: [
                        SwaggerEditorStandalonePreset
                    ],
                    oas3GeneratorUrl: '' //"https://generator3.swagger.io/openapi.json",
                })
                editor.specActions.updateSpec(`{{ spec | replace('`','\`') | safe }}`)
                window.editor = editor;
            };

            save_{{name}} = function() {
                var req = new XMLHttpRequest();
                req.onload = function() {
                    if(this.readyState == 4 && this.status == 200) {
                        alert(this.responseText);
                    } else {
                        alert('Unable to Connect to server.');
                    }
                }
                req.open('POST', "{{ request.url }}", true);
                req.setRequestHeader('content-type', 'application/text;charset=UTF-8');
                {% if csrf_token is defined %}
                req.setRequestHeader("X-CSRFToken", "{{ csrf_token() }}");
                {% endif %}
                req.send(editor.specSelectors.specStr());
            };

            var button = document.createElement("Button");
            button.innerHTML = "Save";
            button.style = "top:0;right:0;margin-top:10px;margin-right:10px;position:absolute;z-index: 9999"
            button.addEventListener("click", save_{{name}}); 
            document.body.appendChild(button);

            </script>
            '''

        scripts.append(flask.render_template_string(custom,spec=self.spec,name=self._safe_name))

        return Markup('\n'.join(scripts))


    def _ui_js(self):
        """
        Load Swagger Editor scripts
        """
        preset = flask.current_app.config.get('SWAGGER_UI_PRESET_JS')
        bundle = flask.current_app.config.get('SWAGGER_UI_BUNDLE_JS')

        if flask.current_app.config['SWAGGER_SERVE_LOCAL']:
            preset = flask.url_for('swagger_editor.static', filename=preset)
            bundle = flask.url_for('swagger_editor.static', filename=bundle)

        scripts = [f'<script src="{url}"></script>\n' for url in [preset, bundle]]

        custom = '''        
            <script>
                var config = {
                "dom_id": "#{{ name }}",
                presets: [
                    SwaggerUIBundle.presets.apis,
                    SwaggerUIStandalonePreset
                ],
                plugins: [
                    SwaggerUIBundle.plugins.DownloadUrl
                ]
                };
                //var user_config = {{config_json|safe}};  // User config options provided from Python code
                //for (var attrname in user_config) { config[attrname] = user_config[attrname]; }
                console.log(config);

                window.onload = function() {
                // Build a system
                const ui = SwaggerUIBundle(config)

                {% if oauth_config_json %}
                    var oauth_user_config = {{oauth_config_json|safe}};  // OAuth2 user config options provided from Python code
                    console.log(oauth_user_config);
                    ui.initOAuth(oauth_user_config);
                {% endif %}

                window.ui = ui
                ui.specActions.updateSpec({{ spec | safe }})
                }
            </script>
            '''

        scripts.append(flask.render_template_string(custom,spec=self.spec,name=self._safe_name))

        return Markup('\n'.join(scripts))

    @property
    def _safe_name(self):
        return ''.join(x.capitalize() or '_' for x in self._name.split('-'))

    def _editor_css(self):
        """
        Load Swagger Editor styles
        """
        swagger_css_url = flask.current_app.config.get('SWAGGER_EDITOR_CSS')
        if flask.current_app.config['SWAGGER_EDITOR_CSS']:
            swagger_css_url = flask.url_for('swagger_editor.static', filename=swagger_css_url)

        swagger_css = f'<link rel="stylesheet" type="text/css" href="{swagger_css_url}">'
        swagger_style = '''
            <style>
            * {
                box-sizing: border-box;
            }
            
            #swagger-editor {
                font-family: Roboto,sans-serif;
                font-size: 9px;
                line-height: 1.42857143;
                color: #444;
                margin: 0px;
            
                font-size: 1.3em;
            }

            .container {
                height: 100%;
                max-width: 880px;
                margin-left: auto;
                margin-right: auto;
            }

            #editor-wrapper {
                height: 100%;
                border:1em solid #000;
                border:none;
            }

            .Pane2 {
                overflow-y: scroll;
            }

            </style>
        '''
        return Markup('\n'.join([swagger_css,swagger_style]))

    def _ui_css(self):
        """
        Load Swagger UI styles
        """
        swagger_css_url = flask.current_app.config.get('SWAGGER_UI_CSS')
        if flask.current_app.config['SWAGGER_UI_CSS']:
            swagger_css_url = flask.url_for('swagger_editor.static', filename=swagger_css_url)

        swagger_css = f'<link rel="stylesheet" type="text/css" href="{swagger_css_url}">'
        swagger_style = '''
        <style>
            html
            {
                box-sizing: border-box;
                overflow: -moz-scrollbars-vertical;
                overflow-y: scroll;
            }
            *,
            *:before,
            *:after
            {
                box-sizing: inherit;
            }

            body {
            margin:0;
            background: #fafafa;
            }
        </style>'''
        return flask.Markup('\n'.join([swagger_css,swagger_style]))
