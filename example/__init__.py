
import flask
import flask_openapi_editor
import pathlib

spec_path = pathlib.Path(__file__).parent / 'openapi.yaml'



app = flask.Flask(__name__)

app.config['SWAGGER_EDITOR_SPEC_URI'] = spec_path

swagger_editor = flask_openapi_editor.SwaggerEditor(
                                            app=app,
                                            editor=True,
                                            editor_path='/')

@swagger_editor.on_save
def save_spec(new_spec):
    import os
    spec_path = flask.current_app.config.get('SWAGGER_EDITOR_SPEC_URI')
    if spec_path and os.path.exists(spec_path):
        with open(spec_path,'w') as f:
            f.write(new_spec)
        return f'Saved to {spec_path}!',200
    
    return 'Set SWAGGER_EDITOR_SPEC_URI to save the spec'

def openapi_blueprint(specification, specification_dir=None, validate_responses=False,
            strict_validation=False, resolver_error=None, auth_all_paths = False, arguments = None,
            base_path='', debug=False,resolver_error_handler=None, validator_map = None, resolver = None):

    import pathlib
    import connexion

    specification_dir = pathlib.Path(specification_dir)
    specification = specification_dir / specification

    resolver = connexion.RestyResolver('blueprints.users.api')

    api = connexion.Api(specification=specification,
                base_path=base_path,
                arguments=arguments,
                resolver=resolver,
                resolver_error_handler=resolver_error_handler,
                validate_responses=validate_responses,
                strict_validation=strict_validation,
                auth_all_paths=auth_all_paths,
                debug=debug,
                validator_map=validator_map)

    return api.blueprint

if __name__ == '__main__':
    app.run(debug=True)